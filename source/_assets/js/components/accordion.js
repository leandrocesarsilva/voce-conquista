class Accordion {
  constructor(elm) {
    this.elm = elm;
    this.item = this.elm.querySelector('[data-item]');
    this.content = this.elm.querySelector('[data-height]');
    this.isOpen = false;
    this.setup();
  }

  setup() {
    this.setMaxHeightDefault();
    this.setupsListeners();
  }

  setMaxHeightDefault() {
    this.content.style.maxHeight = 0;
  }

  setupsListeners() {
    this.item.addEventListener('click', () => { this.setMaxHeight() });
  }

  setMaxHeight() {
    const dataHeight = this.content.firstElementChild.offsetHeight;

    if (this.isOpen) {
      this.content.style.maxHeight = 0;
      this.isOpen = !this.isOpen;
    } else {
      this.content.style.maxHeight = `${dataHeight}px`;
      this.isOpen = !this.isOpen;
    }

    this.item.classList.toggle('is-active');
  }
}

export default {
  create() {
    const elms = Array.from(document.querySelectorAll('[data-accordion]'));
    const instances = [];

    elms.forEach((elm) => {
      instances.push( new Accordion(elm));
    })
  },
};

export const Class = Accordion;