<section class="t-section s-baggage-franchising">
  <div class="c-accordion" data-accordion>
    <div class="c-accordion__title-holder" data-item>
      <img src="assets/images/bag.jpg" alt="">
      <h2 class="c-accordion__title">FRANQUIA DE BAGAGEM</h2>
    </div>
    <div class="c-accordion__container" data-height>
      <div class="c-accordion__content c-flies-table-holder">
        <div class="c-flies-table__title-holder">
          <h3 class="c-flies-table__title">VOOs DOMÉSTICOs</h3>
        </div>
        <ul class="c-flies-table">
          <li class="c-flies-table__row">
            <div class="c-flies-table__row-image-holder">
              <img src="assets/images/latam.jpg" alt="">
            </div>
            <p class="c-flies-table__data-text">
              <strong>Bagagem de mão:</strong> 1 volume de até 10kg e dimensões máximas de 115cm.<br />
              <strong>Bagagem despachada:</strong> 1 volume de até 23kg e dimensões máximas de 158cm.
            </p>
          </li>
          <li class="c-flies-table__row">
            <div class="c-flies-table__row-image-holder">
              <img src="assets/images/gol.jpg" alt="">
            </div>
            <p class="c-flies-table__data-text">
              <strong>Bagagem de mão:</strong> volume de até 10kg e dimensões máximas de 120cm.<br />
              <strong>Bagagem despachada:</strong> 1 volume de até 23kg e dimensões máximas de 158cm.
            </p>
          </li>
          <li class="c-flies-table__row">
            <div class="c-flies-table__row-image-holder">
              <img src="assets/images/azul.jpg" alt="">
            </div>
            <p class="c-flies-table__data-text">
              <strong>Bagagem de mão:</strong> 1 volume de até 10kg e dimensões máximas de 115cm.<br />
              <strong>Bagagem despachada:</strong> 1 volume de até 23 kg e dimensões máximas de 158cm.
            </p>
          </li>
        </ul>
        <div class="c-flies-table__title-holder">
            <h3 class="c-flies-table__title">VOOs internacionais</h3>
          </div>
          <ul class="c-flies-table">
            <li class="c-flies-table__row">
              <div class="c-flies-table__row-image-holder">
                <img src="assets/images/tap.jpg" alt="TAP - Portugal">
              </div>
              <p class="c-flies-table__data-text">
                <strong>Bagagem de mão:</strong><br />
                - <strong>Econômica</strong>: 1 peça de até 8 kg; <strong>Executiva</strong>: 2 peças com peso total combinado de 16 kg.<br />
                <strong>Bagagem despachada:</strong><br />
                <strong>Econômica</strong>: 2 volumes até 23kg por peça; <strong>Executiva</strong>: 2 volumes até 32kg por peça.
              </p>
            </li>
            <li class="c-flies-table__row">
              <div class="c-flies-table__row-image-holder">
                <img src="assets/images/alitalia.jpg" alt="Alitalia">
              </div>
              <p class="c-flies-table__data-text">
                <strong>Bagagem de mão:</strong><br />
                - <strong>Econômica</strong>: 1 peça de até 8 kg; <strong>Executiva</strong>: 2 peças com peso total combinado de 16 kg.<br />
                <strong>Bagagem despachada:</strong><br />
                - <strong>Econômica</strong>: 2 volumes até 23kg por peça; <strong>Executiva</strong>: 2 volumes até 32kg por peça.
              </p>
            </li>
            <li class="c-flies-table__row">
              <div class="c-flies-table__row-image-holder">
                <img src="assets/images/british.jpg" alt="BRITISH AIRWAYS">
                <img src="assets/images/iberia.jpg" alt="IBERIA">
              </div>
              <p class="c-flies-table__data-text">
                <strong>Bagagem de mão:</strong><br />
                - <strong>Econômica</strong>: 1 peça de até 8 kg; Executiva: 2 peças com peso total combinado de 16 kg.<br />
                <strong>Bagagem despachada:</strong><br />
                - <strong>Econômica</strong>: 2 volumes até 23kg por peça; <strong>Executiva</strong>: 2 volumes até 32kg por peça.
              </p>
            </li>
            <li class="c-flies-table__row">
              <div class="c-flies-table__row-image-holder">
                <img src="assets/images/airfrance.jpg" alt="AIRFRANCE">
                <img src="assets/images/klm.jpg" alt="KLM">
              </div>
              <p class="c-flies-table__data-text">
                <strong>Bagagem de mão:</strong><br />
                - <strong>Econômica</strong>: 1 volume de até 12kg; <strong>Executiva</strong>: 1 volume de até 18 kg no máximo.<br />
                <strong>Bagagem despachada:</strong><br />
                - <strong>Econômica</strong>: 2 volumes até 23kg por peça; <strong>Executiva</strong>: 2 volumes até 32kg por peça.
              </p>
            </li>
            <li class="c-flies-table__row">
              <div class="c-flies-table__row-image-holder">
                <img src="assets/images/latam.jpg" alt="LATAM">
              </div>
              <p class="c-flies-table__data-text">
                <strong>Bagagem de mão:</strong><br />
                - <strong>Econômica</strong>: 1 volume de até 8 kg; <strong>Premium Economy ou Premium Business</strong>: 1 volume de até 16 kg (voos que não envolvam o Brasil)<br />
                <strong>Bagagem despachada:</strong><br />
                - <strong>Econômica</strong>: 2 volumes até 23kg por peça; <strong>Executiva</strong>: 2 volumes até 32kg por peça.
              </p>
            </li>
            <li class="c-flies-table__row">
              <div class="c-flies-table__row-image-holder">
                <img src="assets/images/lufthansa.jpg" alt="Lufthansa">
              </div>
              <p class="c-flies-table__data-text">
                <strong>Bagagem de mão:</strong><br />
                - <strong>Econômica</strong>: 1 volume de até 8 kg; <strong>Premium Economy ou Premium Business</strong>: 1 volume de até 16 kg (voos que não envolvam o Brasil)<br />
                <strong>Bagagem despachada:</strong><br />
                - <strong>Econômica</strong>: 2 volumes até 23kg por peça; <strong>Executiva</strong>: 2 volumes até 32kg por peça.
              </p>
            </li>
            <li class="c-flies-table__row">
              <div class="c-flies-table__row-image-holder">
                <img src="assets/images/swiss.jpg" alt="SWISS">
              </div>
              <p class="c-flies-table__data-text">
                <strong>Bagagem de mão:</strong><br />
                - <strong>Econômica</strong>: máx. 8 kg / 17 lbs; <strong>Executiva</strong>: 2 volumes de 8 kg / 17 lbs.<br />
                <strong>Bagagem despachada:</strong><br />
                - <strong>Econômica</strong>: 2 volumes até 23kg por peça; <strong>Executiva</strong>: 2 volumes até 32kg por peça.
              </p>
            </li>
            <li class="c-flies-table__row">
              <div class="c-flies-table__row-image-holder">
                <h4 class="c-flies-table__row-title">Importante</h4>
              </div>
              <ul class="c-important-info">
                <li class="c-important-info__item">• O custo com excesso de bagagem é de responsabilidade do convidado.</li>
                <li class="c-important-info__item">• Passageiros que alteraram seus voos verificar política de bagagem no e-mail enviado com os bilhetes alterados.</li>
                <li class="c-important-info__item">• A dimensão total das bagagens equivale a soma de comprimento, largura e altura - incluindo bolsos, alças e rodinhas.</li>
                <li class="c-important-info__item c-important-info__item--image"><img src="assets/images/bag_highlight.jpg" alt="Largura x Altura x Comprimento"></li>
                <li class="c-important-info__item">• Itens proibidos na bagagem de mão: objetos ou ferramentas pontiagudas e/ou cortantes (incluindo alicates ou tesouras para unhas) ou substâncias inflamáveis/explosivas (incluindo bebidas alcoólicas com graduação superior a 70% de álcool). Apenas é permitido o transporte de LAGs (líquidos, aerossóis e géis) contidos em recipientes individuais de capacidade não superior a 100 mililitros ou equivalente. São permitidos LAGs que se destinarem ao uso durante a viagem e que sejam necessários por razões médicas ou por uma exigência dietética especial.</li>
              </ul>
              <div class="c-important-info__alert-holder">

                @for ($i = 38; $i < 50; $i++)
                  <img src="assets/images/LP_shell_voce-conquista_{{ $i }}.jpg" alt="">
                @endfor

              </div>
            </li>
          </ul>
      </div>
    </div>
  </div>
</section>