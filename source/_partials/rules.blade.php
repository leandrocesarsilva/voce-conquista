<section class="s-documents">
  <div class="c-accordion" data-accordion>
    <div class="c-accordion__title-holder" data-item>
      <img src="assets/images/medkit.jpg" alt="">
      <h2 class="c-accordion__title">DOCUMENTOS OBRIGATÓRIOS PARA EMBARQUE</h2>
    </div>
    <div class="c-accordion__container" data-height>
      <div class="c-accordion__content">
       <div class="s-rules">
         <p>
           <span class="font">Fonte: www.anac.gov.br</span>
           A princípio, não há restrições para o transporte de medicamentos de uso contínuo (ex.: insulina) na bagagem de mão. Entretanto, aconselha-se que o passageiro esteja com a devida prescrição médica que ateste o uso do medicamento. Se necessário, o passageiro deverá entrar em contato com a empresa aérea para averiguar a existência de restrições a certas substâncias e de determinações da autoridade aduaneira do país de destino.<br /><br />

Medicamentos que precisam ser levados na bagagem de mão em voos internacionais não se enquadram nas restrições para transporte de líquidos, mas deverão estar acompanhados da devida prescrição médica. Além disso, devem ser transportados na quantidade necessária ao uso durante todo o voo (incluindo eventuais escalas) e ser apresentados no momento da inspeção de segurança.<br /><br />

O passageiro poderá solicitar, antes do início dos procedimentos de inspeção, que a vistoria dos medicamentos seja feita sem a utilização de equipamentos de raios x e de detectores de metais. Nesse caso, o passageiro deverá entregar os medicamentos de forma separada dos demais itens da bagagem de mão. O procedimento de inspeção deverá ser feito com a utilização do equipamento detector de traços de explosivos (ETD) ou por meio de inspeção manual. Para evitar que os medicamentos sofram contaminação, o próprio passageiro poderá ser solicitado a apresentar, manusear e embalar novamente os medicamentos durante a inspeção.<br /><br />

Mais informações sobre procedimentos de inspeção de segurança em aeroportos brasileiros podem ser encontradas na Resolução ANAC nº 207/2011. <br /><br />

<strong>Assessoria de Imprensa da Anvisa</strong><br />
O tratamento com medicamentos de uso continuado não pode ser interrompido. Então é melhor ter sempre uma nova caixa do remédio antes que o antigo acabe. Lembrando, caso você note alguma característica diferente no remédio que sempre toma, ligue para a Anvisa. Não custa nada e essa informação é muito importante para detectar irregularidades. O telefone é: 0800-647-9782.<br /><br />

Quando viajar, leve seu medicamento com você ao invés de colocá-lo em sua bagagem, pois as malas podem ficar guardadas em locais muito frios ou muito quentes ou até mesmo serem extraviadas. Outro detalhe importante é saber se o remédio que você usa é vendido na cidade para onde você vai. Caso não seja, é melhor levar a quantidade certa para o seu tratamento. Vale levar também uma cópia da receita médica.  Ela pode ser necessária.<br /><br />

O medicamento somente deve ser tomado conforme prescrito pelo seu médico, na hora correta e durante o período de tempo indicado para o tratamento. No caso dos antibióticos, não interrompa o tratamento. Isso pode criar resistência nas bactérias e a doença voltar mais forte e não ceder à medicação. A regularidade também é fundamental no uso das pílulas anticoncepcionais, caso contrário elas não farão efeito.<br /><br />

Importante levar a bula do medicamento.
         </p>
       </div>
      </div>
    </div>
  </div>
</section>