<section class="s-important-info">
  <div class="s-important-info__title-holder">
    <img src="assets/images/lupa.jpg" alt="">
    <h2 class="s-important-info__title">INFORMAÇÕES IMPORTANTES</h2>
  </div>
  <ul class="s-imoportant-info__list">
    <li class="s-imoportant-info__item">• Para viagens internacionais é necessário que seu passaporte esteja válido por no mínimo 6 meses após a data de retorno ao Brasil. Caso verifique que a validade do seu passaporte ou de seu acompanhante não atinge este período, providencie tão logo possível a renovação do seu documento.</li>
    <li class="s-imoportant-info__item">• Favor se atentar as regras de franquia de bagagem para o voo doméstico e internacional (item Franquia de Bagagem).</li>
    <li class="s-imoportant-info__item">• Favor consultar o item DOCUMENTOS OBRIGATÓRIOS PARA EMBARQUE e AUTORIZAÇÃO DE VIAGEM DE MENOR PARA O EXTERIOR.</li>
    <li class="s-imoportant-info__item">• Confira todas as informações para a emissão correta de sua passagem aérea.</li>
  </ul>
</section>