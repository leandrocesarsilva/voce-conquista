<section class="t-section s-documents">
  <div class="c-accordion" data-accordion>
    <div class="c-accordion__title-holder" data-item>
      <img src="assets/images/paper.jpg" alt="">
      <h2 class="c-accordion__title">DOCUMENTOS OBRIGATÓRIOS PARA EMBARQUE</h2>
    </div>
    <div class="c-accordion__container" data-height>
      <div class="c-accordion__content">
       <div class="s-documents__content">
          <p>
            <span class="font">Fonte: www.anac.gov.br</span>
            <span class="italic">Publicado em 13/12/2016, às 14h51. Última modificação em 14/03/2017, às 14h11.</span>

            Ao se preparar para a viagem, verifique a documentação pessoal necessária inclusive a de acompanhantes, especialmente menores de idade. Em voos internacionais, confira as exigências de vacinas e demais regras para estadia no País também.
          </p>
          <ul class="s-documents__list">
            <li class="s-documents__item">
              <p class="s-documents__item-title">Se o passageiro for brasileiro</p>
              <p class="s-documents__item-desc">Em voos domésticos o passageiro pode apresentar   qualquer documento oficial que permita a sua identificação. São aceitas cópias    autenticadas dos documentos.<br /><br />

                Nos voos internacionais para brasileiros, é preciso apresentar passaporte brasileiro válido. No caso de viagens para Argentina, Uruguai, Paraguai, Bolívia, Chile, Peru, Equador, Colômbia e Venezuela, também é aceito como documento de viagem a Carteira de Identidade Civil (RG), emitida pelas Secretarias de Segurança Pública dos Estados ou do Distrito Federal.</p>
            </li>
            <li class="s-documents__item">
              <p class="s-documents__item-title">Se o passageiro for estrangeiro</p>
              <p class="s-documents__item-desc">Em voos domésticos são necessários Passaporte       ou Cédula de Identidade de Estrangeiro - CIE (RNE). São aceitos ainda a           Identidade Diplomática ou a Consular ou outro documento legal de viagem           segundo o Decreto n°5.978/2006 ou resultado de acordos internacionais             firmados pelo Brasil.<br /><br />

                  Nos voos internacionais é obrigatório o Passaporte ou a Carteira de Identidade Civil (RG) para cidadãos dos países do Mercosul.</p>
            </li>
            <li class="s-documents__item">
              <p class="s-documents__item-title">Casos de documentos perdidos ou furtados</p>
              <p class="s-documents__item-desc">Em caso de furto, roubo ou extravio de documento será aceito o Boletim de Ocorrência para embarque em voos domésticos. No caso de voos internacionais, deve ser retirado outro Passaporte. Se a perda se der em território estrangeiro, procure a embaixada do Brasil ou outra representação diplomática brasileira. </p>
            </li>
            <li class="s-documents__item">
              <p class="s-documents__item-title">Crianças e adolescentes</p>
              <p class="s-documents__item-desc">Em voos domésticos é aceita a Certidão de Nascimento (original ou cópia autenticada) ou outro documento de identificação válido para crianças de até 12 anos incompletos. Para adolescentes (entre 12 anos completos e 17 anos) valem as mesmas regras que para os demais passageiros: documento de identificação válido em todo o território nacional. Segundo a Resolução nº 400/2016, a Certidão de Nascimento não é válida para o embarque de adolescentes.<br /><br />

                  Para o embarque de crianças também deve ser apresentado documento que comprove a filiação ou parentesco com o responsável, observadas as demais exigências estabelecidas pelo Estatuto da Criança e do Adolescente e pela Vara da Infância e Juventude do local de embarque.<br /><br />

                  No caso de voos internacionais, o documento de identificação é o Passaporte, além do previsto pelo Estatuto da Criança e do Adolescente, pelo Conselho Nacional de Justiça e pelas determinações da Vara da Infância e Juventude do local de embarque. Importante orientações da Polícia Federal – DPF.</p>
            </li>
          </ul>
          <p>
              <strong>IMPORTANTE! A carteira de estudante não é um documento de identificação previsto para o embarque.<br /><br />

              É possível que crianças e adolescentes viajem desacompanhados dos pais?</strong>

              Em voos domésticos qualquer adolescente (entre 12 anos completos e 18 anos) pode viajar independentemente de autorização dos responsáveis. Crianças (até 12 anos incompletos) estão sujeitas às exigências legais.

              Para embarques domésticos e internacionais é importante consultar a empresa aérea com antecedência e verificar o que diz o Estatuto da Criança e do Adolescente, além das exigências da Vara da Infância e da Juventude da localidade de embarque.<br /><br />

              <strong>AUTORIZAÇÃO DE VIAGEM DE MENOR PARA O EXTERIOR</strong><br />
              Para viajar do Brasil para o exterior, o brasileiro menor de 18 anos que não estiver acompanhado de ambos os pais deve estar munido de <span class="sub">Autorização de Viagem de Menor para o Exterior</span>, formulário pelo qual seus pais permitem-lhe viajar:<br />
              - Em companhia de apenas um deles; ou<br />
              - Em companhia de uma outra pessoa; ou<br />
              - Sob os cuidados de companhia aérea/marítima.<br /><br />

              <strong>COMO SOLICITAR</strong><br />
              É necessário realizar o RECONHECIMENTO DAS ASSINATURAS DOS PAIS no formulário próprio. O reconhecimento pode ser feito gratuitamente no Consulado-Geral do Brasil em Paris, mediante agendamento de horário. Basta seguir o procedimento descrito abaixo:<br /><br />

              <strong>1)</strong> Baixar, imprimir e preencher, em 2 (duas) vias para cada criança, o formulário disponível a seguir:<br />
              • Se a criança for viajar com um dos pais ou acompanhado por terceiro, utilize o <a href="#" target="_blank">Formulário de Autorização de Viagem de Menor para o Exterior de menor acompanhado</a>. É possível colocar fotografias dos menores nos documentos, mas não é obrigatório.<br />
              • Se a criança for viajar desacompanhada dos pais ou de terceiros, utilize o <a href="#" target="_blank">Formulário de Autorização de Viagem de Menor para o Exterior de menor desacompanhado</a>. É possível colocar fotografias dos menores nos documentos, mas não é obrigatório.<br /><br />

              <strong>2)</strong> Reunir a seguinte documentação:<br />
              • Todas as vias dos formulários preenchidas;<br />
              • Os documentos de identificação dos pais; e<br />
              • A certidão de nascimento do menor.<br /><br />

              <strong>ATENÇÃO:</strong> Uma vez reunida toda a documentação acima listada, é necessário <a href="#" target="_blank">agendar horário para atendimento presencial</a>. O simples agendamento do horário não é garantia de prestação do serviço solicitado caso a documentação não esteja completa.
              Alternativamente, o reconhecimento das assinaturas pode ser solicitado junto aos serviços notariais franceses (“mairies” ou “notaires”).<br /><br />

              <strong>COMO UTILIZAR</strong><br />
              • O formulário de Autorização de Viagem de Menor para o Exterior deve ser apresentado ao funcionário do controle de imigração do aeroporto, porto ou posto de fronteira. O Departamento de Polícia Federal exige a apresentação do formulário em 2 (duas) vias originais. Cada jogo de formulários é válido apenas para uma viagem.<br /><br />

              • A Autorização de Viagem de Menor para o Exterior deve ser assinada por ambos os pais, mesmo que apenas um deles detiver a guarda dos filhos. Nos casos em que um dos pais não concordar com a assinatura da Autorização, o outro deverá resolver o impasse pela via judicial.<br /><br />

              • A Autorização de Viagem de Menor para o Exterior não se confunde com a Autorização de Emissão de Passaporte para Menor. São duas autorizações distintas.<br /><br />

              • A Autorização de Viagem de Menor para o Exterior não se aplica a menor estrangeiro. O controle de imigração pode, entretanto, requerer informações sobre a viagem do menor.<br /><br />

              • A Autorização de Viagem de Menor para o Exterior é obrigatória mesmo no caso de menores brasileiros detentores de dupla nacionalidade (brasileira e francesa, por exemplo).<br /><br />

              • A Autorização de Viagem de Menor para o Exterior tem validade de 2 (dois) anos e não constitui autorização para fixação de residência permanente do menor no exterior, salvo disposição expressa em contrário.<br /><br />

              <strong>CASOS ESPECIAIS</strong><br />
              • Se o menor não estiver sob a guarda dos pais, o seu responsável legal deverá assinar o formulário e juntar via original do Termo de Guarda.<br /><br />

              • Se um dos pais do menor for falecido, o genitor sobrevivente deverá assinar o formulário e juntar via original da Certidão de Óbito do genitor falecido.<br /><br />

              • Se um dos pais se encontrar em paradeiro desconhecido, o outro genitor deverá obter, por via judicial, decisão que o autorize a emitir, sozinho, a Autorização de Viagem de Menor para o Exterior.<br /><br />

              <strong>ALTERNATIVAS À AUTORIZAÇÃO DE VIAGEM DE MENOR PARA O EXTERIOR</strong><br />
              É possível, no momento da emissão de um novo passaporte, que os pais do menor solicitem expressamente, no formulário de Autorização de Emissão de Passaporte para Menor, que seja colocada no passaporte a Autorização de Viagem de Menor para o Exterior, dispensando, assim, a preparação dos formulários de Autorização de Viagem de Menor para o Exterior para cada deslocamento internacional do menor. Para maiores detalhes, clique aqui.<br /><br />

              Os pais do menor podem, igualmente, emitir escritura pública autorizando a viagem do menor.<br /><br />

              <strong>NOVA EXIGÊNCIA PARA SAÍDA DE MENORES DA FRANÇA</strong><br />
              As instruções apresentadas acima se referem especificamente à autorização de viagem de menor exigida pelas autoridades migratórias <span class="sub">brasileiras</span>. O Consulado-Geral do Brasil em Paris alerta, contudo, que, a partir de 15 de janeiro de 2017, as autoridades migratórias <span class="sub">franceses</span> também exigirão um formulário de autorização de viagem de menor. Para maiores informações, clique <a href="#" target="_blank">aqui</a>.<br /><br />

              A primeira diferença entre os dois países no que se refere a viagens internacionais de menores é que, no caso da França, o formulário só é obrigatório se o menor estiver viajando sem nenhum dos pais. No caso do Brasil, o formulário sempre é obrigatório se o menor não estiver viajando com ambos os pais, ou seja, o formulário é necessário mesmo que o menor esteja acompanhado pelo pai ou pela mãe. A segunda diferença é que as regras francesas são aplicáveis a todos os menores residentes na França, sejam franceses ou não. As regras brasileiras são, a princípio, limitadas aos menores brasileiros ou binacionais.<br /><br />

              Por fim, é importante também ressaltar que a polícia migratória francesa não está autorizada a aceitar o modelo brasileiro de formulário de autorização de viagem de menor e, da mesma forma, a polícia migratória brasileira não está autorizada a aceitar o modelo francês. Assim, no caso de um menor brasileiro que viaja da França para o Brasil, ele deverá, na ida, apresentar o formulário francês no aeroporto na França e, na volta, apresentar o formulário brasileiro no aeroporto no Brasil.<br /><br />

              Fonte: <a href="http://cgparis.itamaraty.gov.br/pt-br/autorizacao_de_viagem_de_menor_para_o_exterior.xml" target="_blank">http://cgparis.itamaraty.gov.br/pt-br/autorizacao_de_viagem_de_menor_para_o_exterior.xml</a>
          </p>
       </div>
      </div>
    </div>
  </div>
</section>